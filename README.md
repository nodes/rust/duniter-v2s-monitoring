# duniter-v2s-monitoring

1. Generate a node key

```
$ docker run --rm -it --entrypoint duniter duniter/duniter-v2s:v0.4.0 key generate-node-key
12D3KooWSR6aRZm7s9P7ZV9KvyUMn3UtCvPQGVfuYZxSsX4czKAH
1acecb65accd9138fac026d5435fe11243b93b35f1c54218688ffad5159669db
```

The first line should be copy/paste in `.env` at `DUNITER_PEER_ID`.
The second line should be copy/paste in `.env` at `DUNITER_NODE_KEY`.

For example:

```
DUNITER_PEER_ID=12D3KooWSR6aRZm7s9P7ZV9KvyUMn3UtCvPQGVfuYZxSsX4czKAH
DUNITER_NODE_KEY=1acecb65accd9138fac026d5435fe11243b93b35f1c54218688ffad5159669db
```

2. Configure environment variables in `.env` 

You should at least configure `LIBP2P_DOMAIN` to a domain that point to your server.

3. Copy the folder `duniter-node` in your server.

4. At root of `duniter-node` folder (in the remote server), run:

```
docker compose up -d
```

5. Open a ssh bridge to your server to access grafana UI:

```
ssh -L 3000:localhost:3000 user@ip
```

6. Open [http://localhost:3000](http://localhost:3000) (credentials are admin/admin)

7. Import a new dashboard (copy the content of json file `duniter-node/grafana/templates/template.json`). 
